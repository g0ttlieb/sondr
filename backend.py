import sqlite3
import datetime

# Check if login details are valid
def valid_login(user, pswd):

    # Default to true
    valid = True

    # Connect
    conn = sqlite3.connect("general.db")
    c = conn.cursor()

    # Try to get user - if cant find then set to false
    c.execute("SELECT * FROM users WHERE user=? AND pswd=?", (user, pswd))
    if(c.fetchone() == None):
        valid = False

    # Disconnect
    conn.commit()
    conn.close()

    return valid

# Check if already registered
def existing_user(user, email):

    # Default to true
    existing = True

    # Connect
    conn = sqlite3.connect("general.db")
    c = conn.cursor()

    # Try to get user - if cant find then set to false
    c.execute("SELECT * FROM users WHERE user=? AND email=?", (user, email))
    if(c.fetchone() == None):
        existing = False

    # Disconnect
    conn.commit()
    conn.close()

    return existing

# Check if username taken
def taken_username(user):

    # Default to true
    taken = True

    # Connect
    conn = sqlite3.connect("general.db")
    c = conn.cursor()

    # Try to get user - if cant find then set to false
    c.execute("SELECT * FROM users WHERE user=?", (user,))
    if(c.fetchone() == None):
        taken = False

    # Disconnect
    conn.commit()
    conn.close()

    return taken


# Ensure valid password: at least 8 characters long
def invalid_pswd(pswd):
    if(len(pswd) < 8):
        return True
    return False

# Add new user
def add_user(user, pswd, email):

    # Connect to general database
    conn = sqlite3.connect("general.db")
    c = conn.cursor()

    # Add new database entry
    c.execute("INSERT INTO users VALUES(?, ?, ?, ?, ?)", (user, pswd, email, 0, 0))

    # Disconnect from general database
    conn.commit()
    conn.close()

    # -----

    # Connect to users database
    conn = sqlite3.connect("users.db")
    c = conn.cursor()

    # Create new private table
    c.execute("CREATE TABLE {} (status text, ID int, name text, topic text, date text, bio text, message text, matching_ID int)".format(user))

    #Disconnect from users database
    conn.commit()
    conn.close()


# Return users notifications
def get_notifications(user):

    # Connect
    conn = sqlite3.connect("general.db")
    c = conn.cursor()

    # Extract notifications columns from user entry
    c.execute("SELECT new_matches, new_completed FROM users WHERE user=?", (user, ))
    entry = c.fetchone()
    new_matches = entry[0]
    new_completed = entry[1]

    # Disconnect
    conn.commit()
    conn.close()

    return [new_matches, new_completed]

def reset_newly_completed(user):
    # Connect
    conn = sqlite3.connect("general.db")
    c = conn.cursor()

    # Reset new_completed to 0
    c.execute("UPDATE users SET new_completed = 0 WHERE user=?", (user, ))

    # Disconnect
    conn.commit()
    conn.close()

def reset_new_matches_found(user):
    # Connect
    conn = sqlite3.connect("general.db")
    c = conn.cursor()

    # Reset new_matches to 0
    c.execute("UPDATE users SET new_matches = 0 WHERE user=?", (user, ))

    # Disconnect
    conn.commit()
    conn.close()

def get_topics():
    topics = []
    topics.append("Food")
    topics.append("Fun Facts")
    topics.append("Games")
    topics.append("General Recommendations")
    topics.append("Life Advice")
    topics.append("Movies")
    topics.append("Music")
    topics.append("TV Shows")
    topics.append("None of the Above")
    return topics

def add_search(user, topic, bio):

    # Connect to databases
    connGeneral = sqlite3.connect("general.db")
    cGen = connGeneral.cursor()
    connUsers = sqlite3.connect("users.db")
    cUser = connUsers.cursor()

    # New ID
    cUser.execute("SELECT MAX(ID) FROM {}".format(user))
    ID = cUser.fetchone()[0]
    if(ID == None):
        ID = 0
    else:
        ID += 1

    # Get current date
    now = datetime.datetime.now()
    date = str(now.day) + " / " + str(now.month) + " / " + str(now.year)

    # Try to find existing search with that topic
    cGen.execute("SELECT * FROM searches WHERE topic=? AND user!=?", (topic, user))
    # Entry = topic user bio ID
    entry = cGen.fetchone()
    # If no found match: add this search to database
    if(entry == None):
        # Add to current searches database
        cGen.execute("INSERT INTO searches VALUES(?, ?, ?, ?)", (topic, user, bio, ID))

        # Add to private searches database
        cUser.execute("INSERT INTO {} VALUES('searching',?,'',?,?,'','',0)".format(user), (ID, topic, date))

    # Otherwise: facilitate match between the 2
    else:
        # Remove entry from searches db
        cGen.execute("DELETE FROM searches WHERE topic=? AND user=? AND bio=? AND ID=?", entry)

        # Add to private searches database of new searcher
        cUser.execute("INSERT INTO {} VALUES('current',?,?,?,?,?,'',?)".format(user), (ID, entry[1], topic, date, entry[2], entry[3]))

        # Update search in old searcher
        cUser.execute("UPDATE {} SET status='current', name=?, date=?, bio=?, matching_ID=? WHERE ID=?".format(entry[1]), (user, date, bio, ID, entry[3]))

        # Increment new_matches of new searcher
        cGen.execute("SELECT new_matches FROM users WHERE user=?", (user,))
        new_matches = cGen.fetchone()[0] + 1
        cGen.execute("UPDATE users SET new_matches = ? WHERE user=?", (new_matches, user))

        # Increment new_matches of old searcher
        cGen.execute("SELECT new_matches FROM users WHERE user=?", (entry[1],))
        new_matches = cGen.fetchone()[0] + 1
        cGen.execute("UPDATE users SET new_matches = ? WHERE user=?", (new_matches, entry[1]))

    # Disconnect
    connGeneral.commit()
    connGeneral.close()
    connUsers.commit()
    connUsers.close()

# Get all completed searches
# Format: Name, Topic, Date, Bio, Your Message, Their Message
def get_history(user):

     # Connect
    conn = sqlite3.connect("users.db")
    c = conn.cursor()

    history = [entry for entry in c.execute("SELECT name, topic, date, bio, message FROM {} WHERE status = 'complete'".format(user))]

    # Disconnect
    conn.commit()
    conn.close()

    return history

# Get all current searches
# Format: Topic, Date, Status, Name, Bio, ID
def get_current(user):

    # Connect
    conn = sqlite3.connect("users.db")
    c = conn.cursor()

    current = [entry for entry in c.execute("SELECT topic, date, status, name, bio, ID FROM {} WHERE status != 'complete'".format(user))]

    # Disconnect
    conn.commit()
    conn.close()

    return current

# Update current search
def update_current(user, ID, message):

    # Connect
    connUsers = sqlite3.connect("users.db")
    cUsers = connUsers.cursor()
    connGeneral = sqlite3.connect("general.db")
    cGen = connGeneral.cursor()

    # Get current date
    now = datetime.datetime.now()
    date = str(now.day) + " / " + str(now.month) + " / " + str(now.year)

    # Get relevant entries
    cUsers.execute("SELECT * FROM {} WHERE ID=?".format(user), (ID,))
    entry1 = cUsers.fetchone()
    cUsers.execute("SELECT * FROM {} WHERE ID=?".format(entry1[2]), (entry1[7], ))
    entry2 = cUsers.fetchone()
    print(entry1)
    print(entry2)

    # If they have sent their message: update with new message + date and set to complete
    # Change which bio is stored
    if(entry2[0] == 'limbo'):

        # Update entries
        cUsers.execute("UPDATE {} SET status='complete', date=?, bio=? WHERE ID=?".format(user), (date, entry2[5], entry1[1]))
        cUsers.execute("UPDATE {} SET status='complete', date=?, message=?, bio=? WHERE ID=?".format(entry1[2]), (date, message, entry1[5], entry2[1]))

        # Increment new_completed of current user
        cGen.execute("SELECT new_completed FROM users WHERE user=?", (user,))
        new_completed = cGen.fetchone()[0] + 1
        cGen.execute("UPDATE users SET new_completed = ? WHERE user=?", (new_completed, user))

        # Increment new_completed of other user
        cGen.execute("SELECT new_completed FROM users WHERE user=?", (entry1[2],))
        new_completed = cGen.fetchone()[0] + 1
        cGen.execute("UPDATE users SET new_completed = ? WHERE user=?", (new_completed, entry1[2]))

    # If they haven't: update with new message + date and set to limbo
    else:

        # Update entries
        cUsers.execute("UPDATE {} SET status='limbo', date=? WHERE ID=?".format(user), (date, entry1[1]))
        cUsers.execute("UPDATE {} SET message=? WHERE ID=?".format(entry1[2]), (message, entry2[1]))

    # Disconnect
    connUsers.commit()
    connUsers.close()
    connGeneral.commit()
    connGeneral.close()
