from flask import Flask, request, render_template, make_response, redirect, session
from flask_session import Session
from jinja2 import utils
from backend import *

# Initialisation
app = Flask(__name__)
sess = Session()

# ----------------------------------------------------------------------------------------------------
# LOGIN
# ----------------------------------------------------------------------------------------------------

# Front page: login / register
@app.route("/", methods=['POST', 'GET'])
def login():

    status = request.cookies.get("status")

    resp = make_response(render_template("login.html", status=status))
    resp.set_cookie("status", "none")

    session.clear()

    return resp


# Verify Login
@app.route("/verify_login/", methods=['POST', 'GET'])
def verify_login():
    user = request.form["user"]
    pswd = request.form["pswd"]
    if valid_login(user, pswd):
        session['user'] = user
        return redirect("/home/")
    resp = make_response(redirect("/"))
    resp.set_cookie("status", "invalid_login")
    return resp


# Verify registration
@app.route("/register/", methods=['POST', 'GET'])
def register():

    user = request.form["user"]
    pswd = request.form["pswd"]
    email = request.form["email"]

    resp = make_response(redirect("/"))
    
    if user != str(utils.escape(user)):
        resp.set_cookie("status", "invalid_username")
    elif existing_user(user, email):
        resp.set_cookie("status", "existing_user")
    elif taken_username(user):
        resp.set_cookie("status", "username_taken")
    elif invalid_pswd(pswd):
        resp.set_cookie("status", "invalid_password")
    else:
        add_user(user, pswd, email)
        session['user'] = user
        return redirect("/home/")
    return resp


# ----------------------------------------------------------------------------------------------------
# HOME
# ----------------------------------------------------------------------------------------------------

# Home page
@app.route("/home/", methods=['POST', 'GET'])
def home():

    if "user" not in session:
        return redirect("/")

    user = session['user']

    notifications = get_notifications(user)

    return render_template("home.html", user=user, notifications=notifications)

# ----------------------------------------------------------------------------------------------------
# New Search
# ----------------------------------------------------------------------------------------------------

# New search
@app.route("/new_search/", methods=['POST', 'GET'])
def new_search():

    if "user" not in session:
        return redirect("/")

    user = session['user']

    topics = get_topics()

    status = request.cookies.get("status")

    resp = make_response(render_template("new_search.html", status=status, topics=topics))
    resp.set_cookie("status", "none")

    return resp


@app.route("/new_search/process/", methods=['POST'])
def process_search():

    topics = get_topics()

    topic = request.form["topic"]

    if topic not in topics:
        resp = make_response(redirect("/new_search/"))
        resp.set_cookie("status", "invalid_topic")
        return resp

    bio = request.form["bio"]

    add_search(session['user'], topic, bio)

    return redirect("/home/")

# ----------------------------------------------------------------------------------------------------
# Current
# ----------------------------------------------------------------------------------------------------

# Currently active searches
@app.route("/current/", methods=['POST', 'GET'])
def current():

    if "user" not in session:
        return redirect("/")

    user = session['user']

    current = get_current(user)

    reset_new_matches_found(user)

    return render_template("current.html", current=current)

@app.route("/current/process/", methods=['POST', 'GET'])
def process_current():
    id = request.form["id"]
    user = session['user']
    message = request.form["message"]

    update_current(user, id, message)

    return redirect("/current/")

# ----------------------------------------------------------------------------------------------------
# History
# ----------------------------------------------------------------------------------------------------

# History
@app.route("/history/", methods=['POST', 'GET'])
def history():

    if "user" not in session:
        return redirect("/")

    user = session['user']

    history = get_history(user)

    reset_newly_completed(user)

    return render_template("history.html", history=history)


# Init Session
app.secret_key = '\xff\xa8\x80H\xab\x02\x1b\x93"\x85B&,\xe3 .\xc4\xe1!\\ EN\x16k\x0f\x1c\xa3\'{\x8aC'
app.config['SESSION_TYPE'] = 'filesystem'
sess.init_app(app)

# Run app
if __name__ == '__main__':
    app.run()
