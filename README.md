# sondR

A social network with the lights off.

sondR is a new type of social media platform that does away with all of the fluff
that plagues our online social footprint. It facilitates the fleating anonymous connection between
two people via a common interest so that they can share and receive heartfelt
recommendations on topics that they are interested in!

Hosted at: [sondr.herokuapp.com](https://sondr.herokuapp.com/)

# Suggestions

1. Change sqlite3 to postgres for better heroku integration
    - Same syntax?
    - psycopg2
2. Admin account

# A Note from 2019
Holy moly what was going with with those databases! I have since got proper db
training!
